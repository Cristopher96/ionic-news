import { Component, ViewChild, OnInit } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { NoticiasService } from '../../services/noticias.service';
import { Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  @ViewChild(IonSegment, {static: true}) segment: IonSegment;

  categories = ['business', 'entertainment', 'general',
   'health', 'science', 'sports', 'technology'];
  noticias: Article[] = [];
  constructor(private newsService: NoticiasService) {}

  ngOnInit() {
    this.segment.value = this.categories[0];
    this.uploadNews(this.segment.value);
  }

  changeCategory(e) {
    this.noticias = [];
    this.uploadNews(e.detail.value);
  }

  uploadNews(category: string, e?) {
    this.newsService.getTopHeadlinesCategory(category).subscribe(resp => {
        if (resp.articles.length === 0) {
          // e.target.disabled = true;
          // e.target.complete();
          return;
        }
        this.noticias.push(...resp.articles);
        // if (e) {
        //   e.target.complete();
        // }
      });
  }

  loadData(e) {
    this.uploadNews(this.segment.value, e);
  }
}
