import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Article } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticias: Article[] = [];
  constructor(private storage: Storage) {
    this.loadFavorites();
  }

  saveNew(n: Article) {
    const exists = this.noticias.find(noti => noti.title === n.title);
    if (!exists) {
      this.noticias.unshift(n);
      this.storage.set('favorites', this.noticias);
    }
  }

  async loadFavorites() {
    const favorites = await this.storage.get('favorites');
    if (favorites) {
      this.noticias = favorites;
    }
  }

  deleteNew(n: Article) {
    this.noticias = this.noticias.filter(noti => noti.title !== n.title);
    this.storage.set('favorites', this.noticias);
  }

}
