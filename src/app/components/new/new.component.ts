import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../../interfaces/interfaces';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController, ToastController, Platform } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
})
export class NewComponent implements OnInit {

  @Input() noticia: Article;
  @Input() index: number;
  @Input() inFavorite;
  constructor(
    private iab: InAppBrowser,
    private actionSheetCtrl: ActionSheetController,
    private socialSharing: SocialSharing,
    private dataLocalService: DataLocalService,
    private toastCtrl: ToastController,
    private platform: Platform
  ) { }

  ngOnInit() {}

  async showToast(m: string) {
    const toast = await this.toastCtrl.create({
      message: m,
      duration: 1500
    });
    toast.present();
  }

  openNew() {
    const browser = this.iab.create(this.noticia.url, '_system');
  }

  async launchMenu() {
    let saveDeleteBtn;
    if (this.inFavorite) {
      // delete
      saveDeleteBtn = {
          text: 'Delete from favorite',
          icon: 'trash',
          cssClass: 'action-dark',
          handler: () => {
            this.dataLocalService.deleteNew(this.noticia);
            this.showToast('Article deleted from favorite');
          }
        };
    } else {
      // add
      saveDeleteBtn = {
          text: 'Favorite',
          icon: 'star',
          cssClass: 'action-dark',
          handler: () => {
            this.showToast('Article added to favorite');
            this.dataLocalService.saveNew(this.noticia);
          }
        };
    }
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Share',
          icon: 'share',
          cssClass: 'action-dark',
          handler: () => {
            this.shareArticle();
          }
        },
        saveDeleteBtn,
        {
          text: 'Cancel',
          icon: 'close',
          cssClass: 'action-dark',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    await actionSheet.present();
  }

  shareArticle() {
    if (this.platform.is('cordova')) {
    this.socialSharing.share(this.noticia.title, this.noticia.source.name, '', this.noticia.url);
    } else {
      // tslint:disable-next-line: no-string-literal
      if (navigator['share']) {
        // tslint:disable-next-line: no-string-literal
        navigator['share']({
          title: this.noticia.title,
          text: this.noticia.description,
          url: this.noticia.url,
        }).then(() => console.log('Successful share')).catch((error) => console.log('Error sharing', error));
      } else {
        console.log('Your web browser dont support the Web Share API');
      }
    }
  }
}
